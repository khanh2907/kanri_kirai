class <%= class_name.pluralize %>Controller < ApplicationController
  before_action :set_post, only: [:show]

  def index
    @<%= class_name.pluralize.downcase %> = <%= class_name %>.paginate(page: params[:page], per_page: 5).order('created_at DESC')
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @file_viewer = @<%= class_name.downcase %>.file_viewer("#{request.protocol}#{request.host}:#{request.port}")
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @<%= class_name.downcase %> = <%= class_name %>.find(params[:id])
  end
end
