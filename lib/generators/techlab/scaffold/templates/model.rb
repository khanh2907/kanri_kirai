class <%= class_name %> < ActiveRecord::Base
  include Bootsy::Container
  searchkick
  mount_uploader :file, DocumentUploader

  def file_viewer(root)
    return nil if file.nil?
    if file.file.extension == 'pdf'
      {type: 'pdf' , src: file.url}
    else
      {type: file.file.extension , src: "https://docs.google.com/viewer?url=#{root}#{file.url}&embedded=true"}
    end
  end

  def path
    self
  end
end
