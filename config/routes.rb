Rails.application.routes.draw do
	resources :publications, :only => [:index, :show] # techlab_cms generated

	#Casein routes
	namespace :casein, path: 'admin' do
		resources :publications
		resources :readings
	end

	resources :readings, :only => [:index, :show] # techlab_cms generated
  get 'search', to: 'search#index', as: 'search'
  get 'tag/:tag', to: 'tags#show', as: 'tag'
  root to: 'visitors#index'
  mount Bootsy::Engine => '/bootsy', as: 'bootsy'
end
