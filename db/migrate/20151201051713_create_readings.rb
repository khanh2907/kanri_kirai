class CreateReadings < ActiveRecord::Migration
  def self.up
    create_table :readings do |t|
      t.string :title
t.text :content
t.text :tags
t.string :file

    t.timestamps
end
end

def self.down
  drop_table :readings
  end
end