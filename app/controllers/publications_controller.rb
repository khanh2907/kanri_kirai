class PublicationsController < ApplicationController
  before_action :set_post, only: [:show]

  def index
    @publications = Publication.paginate(page: params[:page], per_page: 5).order('created_at DESC')
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @file_viewer = @publication.file_viewer("#{request.protocol}#{request.host}:#{request.port}")
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @publication = Publication.find(params[:id])
  end
end
