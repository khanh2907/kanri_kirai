class ApplicationController < ActionController::Base
  include Rails.application.routes.url_helpers

  protect_from_forgery with: :exception

  before_action :set_recent_content, :set_search

  private
  def set_recent_content
    @recent_content = []

    CMS_MODELS.each do |cms_model|
      @recent_content += cms_model.last(5).map do |entity|
        Content.new(title: entity.title, content: entity.content, created_at: entity.created_at, path: send("#{cms_model.name.downcase}_path", entity), model_name: cms_model.name, tags: entity.tags)
      end
    end

    @recent_content = @recent_content.sort_by(&:created_at).reverse
    @recent_content = @recent_content[0..(4)]
  end

  def set_search
    @search = Search.new unless params[:controller] == 'search'
  end
end
