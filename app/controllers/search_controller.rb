class SearchController < ApplicationController
  require 'will_paginate/array'
  def index
    @is_search = true
    @search = Search.new(search_params)
    @search.categories = @search.categories.delete_if { |t| t.blank? } unless @search.categories.nil?
    @search.tags = @search.tags.delete_if { |t| t.blank? } unless @search.tags.nil?

    search_results = []

    models_to_search = @search.categories.empty? ? CMS_MODELS : @search.categories.map {|c| CMS_MODELS[c.to_i]}
    query = @search.query.blank? ? '*' : @search.query

    query += " " + @search.tags.join(' ') unless @search.tags.nil? or @search.tags.empty?

    models_to_search.each do |cms_model|
      cms_model.search(query).each do |entity|
        search_results.push Content.new(title: entity[:title],
                                content: entity[:content],
                                created_at: entity.created_at,
                                path: send("#{cms_model.name.downcase}_path",entity),
                                model_name: cms_model.name, tags: entity.tags)
      end
    end

    search_results = search_results.sort_by(&:created_at).reverse

    @results_count = search_results.count

    @results = search_results.paginate(:page => params[:page], :per_page => 5)
    respond_to do |format|
      format.html
      format.js
    end
  end

  private
  def search_params
    params.require(:search).permit(:query, :categories => [], :tags => [])
  end
end
