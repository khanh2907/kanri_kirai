# Scaffolding generated by Casein v1.3.3.7

module Casein
  class ReadingsController < Casein::CaseinController

    ## optional filters for defining usage according to Casein::AdminUser access_levels
    # before_filter :needs_admin, :except => [:action1, :action2]
    # before_filter :needs_admin_or_current_user, :only => [:action1, :action2]
  
    def index
      @casein_page_title = 'Readings'
      @readings = Reading.order(sort_order(:title)).paginate :page => params[:page]
    end
  
    def show
      @casein_page_title = 'View reading'
      @reading = Reading.find params[:id]
      @tags = @reading.tags.split(', ')
    end
  
    def new
      @casein_page_title = 'Add a new reading'
      @reading = Reading.new
    end

    def create
      @reading = Reading.new reading_params

      @reading.tags = reading_tags.join(', ')

      if @reading.save
        Tag.update_and_clean([], reading_tags)

        flash[:notice] = 'Reading created'
        redirect_to casein_readings_path
      else
        flash.now[:warning] = 'There were problems when trying to create a new reading'
        render :action => :new
      end
    end

    def update
      @casein_page_title = 'Update reading'

      @reading = Reading.find params[:id]
      old_tags = @reading.tags.split(', ')
      @reading.tags = reading_tags.join(', ')

      if @reading.update_attributes reading_params
        Tag.update_and_clean(old_tags, reading_tags)
        flash[:notice] = 'Reading has been updated'
        redirect_to casein_readings_path
      else
        flash.now[:warning] = 'There were problems when trying to update this reading'
        render :action => :show
      end
    end

    def destroy
      @reading = Reading.find params[:id]
      Tag.update_and_clean(@reading.tags.split(', '), [])
      @reading.destroy
      flash[:notice] = 'Reading has been deleted'
      redirect_to casein_readings_path
    end

    private
      
      def reading_params
        params.require(:reading).permit(:bootsy_image_gallery_id, :title, :content, :tags, :file)
      end

      def reading_tags
        if params[:reading][:tags]
          params[:reading][:tags].delete_if { |t| t.blank? }
        else
          []
        end
      end
  
  end
end
