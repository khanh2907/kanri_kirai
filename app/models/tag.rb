class Tag < ActiveRecord::Base
  validates_uniqueness_of :name
  # decrement the tag count and if it's zero delete it
  def decrement_or_delete
    self.count = self.count - 1
    if self.count <= 0
      self.delete
    else
      self.save
    end
  end

  # invoked when a post is created or updated
  def self.update_and_clean(old_tags, new_tags)
    to_add = new_tags - old_tags
    to_decrement = old_tags - new_tags

    to_decrement.each do |tag|
      find_by(name: tag).decrement_or_delete
    end

    to_add.each do |tag|
      tag = find_or_create_by(name: tag) do |t|
        t.count = 0
      end
      tag.update(count: tag.count + 1)
    end
  end
end
